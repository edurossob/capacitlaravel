<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Empresa;

class Colaborador extends Model
{
	/**
	*
	*	@var array
	*/

    protected $fillable = ['nome' , 'idade'];

    public function empresa(){
    	return $this->belongsTo(Empresa::class);
    }

    public function salario(){
    	return $this->hasOne(Salario::class);
    }

    protected $table = 'colaboradores';
}
