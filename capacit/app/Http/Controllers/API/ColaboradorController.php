<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Empresa;
use App\Colaborador;
use Validator;

class ColaboradorController extends BaseController
{
	public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'empresa_id' => 'required|integer',
            'nome' => 'required|max:255',
            'idade' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Erro de validação', $validate->errors());
        }

        $empresa = Empresa::find($request->empresa_id);
        if(!$empresa){
        	return $this::enviarRespostaErro('Empresa não encontrada');
        }

        $colaborador = new Colaborador;
        $colaborador->nome = $request -> nome;
        $colaborador->idade = $request -> idade;
        $empresa->$empresa->colaboradores()->save($colaborador);

        return $this::enviarRespostaSucesso($colaborador, 'Colaborador crado com sucesso', 201);
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'empresa_id' => 'required|integer',
            'colaborador_id' => 'required|integer',
            'nome' => 'required|max:255',
            'idade' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $colaborador = Colaborador::find($request->colaborador_id);
        $empresa = Empresa::find($request->empresa_id);
        if(!$grupo || !$empresa){
            return $this::enviarRespostaErro('Empresa ou colaborador não encontrado.');
        }

        $colaborador->empresa()->associate($empresa);
        $colaborador->nome = $request->nome;
        $colaborador->idade = $request->idade;
        $colaborador->save();

        return $this::enviarRespostaSucesso($grupo, 'Colaborador alterado com sucesso', 200);
    }

    public function destroy(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $colaborador = Colaborador::find($request->id);
        if(!$colaborador){
            return $this::enviarRespostaErro('Colaborador não encontrado.');
        }

        $colaborador->delete();

        return $this::enviarRespostaSucesso($colaborador, 200);
    }

    public function show(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $colaborador = Colaborador::find($request->id);
        if(!$colaborador){
            return $this::enviarRespostaErro('Colaborador não encontrado.');
        }

        return $this::enviarRespostaSucesso($colaborador, 200);
    }

    public function index(){
        $validate = Validator::make($request->all(), [
            'empresa_id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $empresa = Empresa::find($request->id);
        if(!$empresa){
            return $this::enviarRespostaErro('Empresa não encontrada.', null);
        }

        if($empresa->colaboradores->count() > 0){
            return $this::enviarRespostaSucesso($empresa->colaboradores);
        }
        return $this::enviarRespostaErro('Não há colaboradores cadastrados');
    }

}
