<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Empresa;
use App\Grupo;
use Validator;

class EmpresaController extends BaseController
{
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'grupo_id' => 'required|integer',
            'nome' => 'required|max:255',
            'endereco' => 'required|max:255'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $grupo = Grupo::find($request->grupo_id);
        if(!$grupo){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }   

        $empresa = new Empresa;
        $empresa->nome = $request -> nome;
        $empresa->endereco = $request -> endereco;
        $empresa->save();

        $empresa->grupos()->attach($grupo);

        return $this::enviarRespostaSucesso($empresa, 'Empresa criada com sucesso', 200);
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id' => 'required|longint',
            'nome' => 'required|max:255',
            'endereco' => 'required|max:255'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $empresa = Empresa::find($request->id);
        if(!$empresa){
            return $this::enviarRespostaErro('Empresa não encontrada.');
        }

        $empresa->nome = $request->nome;
        $empresa->save();

        return $this::enviarRespostaSucesso($empresa, 'Empresa criada com sucesso', 200);
    }

    public function removeGrupo(Request $request){
        $validate = Validator::make($request->all(), [
            'grupo_id' => 'required|integer',
            'empresa_id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $empresa = Empresa::find($request->id);
        $grupo = Grupo::find($request->id);
        if(!$empresa || grupo){
            return $this::enviarRespostaErro('Empresa não encontrada.');
        }

        $empresa->grupos()->detach($grupo);

        return $this::enviarRespostaSucesso($empresa, 'Grupo não está mais relacionado à empresa.', 200);
    }

    public function destroy(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $empresa = Empresa::find($request->id);
        if(!$empresa){
            return $this::enviarRespostaErro('Empresa não encontrada.');
        }

        $empresa->delete();

        return $this::enviarRespostaSucesso($empresa, 200);
    }

    public function show(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $empresa = Empresa::find($request->id);
        if(!$empresa){
            return $this::enviarRespostaErro('Empresa não encontrada.');
        }

        return $this::enviarRespostaSucesso($empresa, 200);
    }

    public function index(){
        $validate = Validator::make($request->all(), [
            'grupo_id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $grupo = Grupo::find($request->id);
        if(!$grupo){
            return $this::enviarRespostaErro('Grupo não encontrado.', null);
        }

        if($grupo->empresas->count() > 0){
            return $this::enviarRespostaSucesso($grupo->empresas);
        }
        return $this::enviarRespostaErro('Não há empresas cadastradas');

    }
    
}
