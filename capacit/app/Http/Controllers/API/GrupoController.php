<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Grupo;
use Validator;

class GrupoController extends BaseController
{
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nome' => 'required|max:255'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $grupo = new Grupo;
        $grupo->nome = $request -> nome;
        $grupo->save();

        return $this::enviarRespostaSucesso($grupo, 'Grupo crado com sucesso', 200);
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id' => 'required|longint',
            'nome' => 'required|max:255'   
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $grupo = Grupo::find($request->id);
        if(!$grupo){
            return $this::enviarRespostaErro('Grupo não encontrado.');
        }

        $grupo->nome = $request->nome;
        $grupo->save();

        return $this::enviarRespostaSucesso($grupo, 'Grupo crado com sucesso', 200);
    }

    public function destroy(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $grupo = Grupo::find($request->id);
        if(!$grupo){
            return $this::enviarRespostaErro('Grupo não encontrado.');
        }

        $grupo->delete();

        return $this::enviarRespostaSucesso($grupo, 200);
    }

    public function show(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $grupo = Grupo::find($request->id);
        if(!$grupo){
            return $this::enviarRespostaErro('Grupo não encontrado.');
        }

        return $this::enviarRespostaSucesso($grupo, 200);
    }

    public function index(){
        $grupo = Grupo::all();
        return $this::enviarRespostaSucesso($grupo, 200);
    }

}
