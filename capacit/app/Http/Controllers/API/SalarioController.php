<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Salario;
use App\Colaborador;
use Validator;

class SalarioController extends BaseController
{
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'colaborador_id' => 'required|integer',
            'valor' => 'required|max:255'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Erro de validação', $validate->errors());
        }

        $colaborador = Colaborador::find($request->empresa_id);
        if(!$colaborador){
        	return $this::enviarRespostaErro('Empresa não encontrada');
        }

        $salario = new Salario;
        $salario->valor = $request -> nome;
        $colaborador->$colaborador->salario()->save($salario);

        return $this::enviarRespostaSucesso($salario, 'salario criado com sucesso', 201);
    }

    public function update(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'salario_id' => 'required|integer',
            'colaborador_id' => 'required|integer',
            'valor' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $salario = Salario::find($request->salario_id);
        $colaborador = Colaborador::find($request->empresa_id);
        if(!$salario || !$colaborador){
            return $this::enviarRespostaErro('Colaborador ou salario não encontrado.');
        }

        $salario->colaborador()->associate($colaborador);
        $salario->valor = $request->valor;
        $salario->save();

        return $this::enviarRespostaSucesso($salario, 'salario alterado com sucesso', 200);
    }

    public function destroy(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $salario = Salario::find($request->id);
        if(!$salario){
            return $this::enviarRespostaErro('salario não encontrado.');
        }

        $salario->delete();

        return $this::enviarRespostaSucesso($salario, 200);
    }

    public function show(Request $request){
        $validate = Validator::make($request->all(), [
            'id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $salario = Salario::find($request->id);
        if(!$salario){
            return $this::enviarRespostaErro('salario não encontrado.');
        }

        return $this::enviarRespostaSucesso($salario, 200);
    }

    public function index(){
        $validate = Validator::make($request->all(), [
            'empresa_id' => 'required|integer'
        ]);

        if($validate->fails()){
            return $this::enviarRespostaErro('Campo incorreto', $validate->errors());
        }

        $colaborador = Colaborador::find($request->id);
        if(!$colaborador){
            return $this::enviarRespostaErro('Colaborador não encontrado.', null);
        }

        if($colaborador->salario->count() > 0){
            return $this::enviarRespostaSucesso($colaborador->salario);
        }
        return $this::enviarRespostaErro('Não há empresas cadastradas');
    }

}
