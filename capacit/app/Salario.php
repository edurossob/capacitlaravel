<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Empresa;

class Salario extends Model
{
	/**
	*
	*	@var array
	*/
    protected $fillable = ['valor' , 'colaborador_id'];

    public function colaborador(){
    	return $this->belongsTo(Colaborador::class);
    }
}
