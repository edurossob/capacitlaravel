<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user -> nome = 'Admin';
        $user -> email = 'admin@ecomp.co';
        $user -> senha = bcrypt('secret');
        $user -> admin = true;
        $user -> email_verificado = true;
        $user -> save();


        $user = new User;
        $user -> nome = 'User';
        $user -> email = 'user@ecomp.co';
        $user -> senha = bcrypt('secret');
        $user -> email_verificado = true;
        $user -> save();
 
    }
}
