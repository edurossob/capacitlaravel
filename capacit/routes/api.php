<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\AuthController@login');
Route::post('registro', 'API\AuthController@registro');

//Route::middleware('auth:api')->group(function(){

	Route::post('showGrupo', 'API\GrupoController@show');
	Route::get('indexGrupo', 'API\GrupoController@index');

	Route::post('showEmpresa', 'API\EmpresaController@show');
	Route::post('indexEmpresa', 'API\EmpresaController@index');

	Route::post('showColaborador', 'API\ColaboradorController@show');
	Route::post('indexColaborador', 'API\ColaboradorController@index');

	Route::post('showSalario', 'API\SalarioController@show');
	Route::post('indexSalario', 'API\SalarioController@index');

	Route::post('logout', 'API\AuthController@logout');

	Route::middleware('admin')->group(function(){
		
		Route::post('criaGrupo', 'API\GrupoController@store');
		Route::post('atualizaGrupo', 'API\GrupoController@update');
		Route::post('deleteGrupo', 'API\GrupoController@destroy');

		Route::post('criaEmpresa', 'API\EmpresaController@store');
		Route::post('atualizaEmpresa', 'API\EmpresaController@update');
		Route::post('deleteEmpresa', 'API\EmpresaController@destroy');
		Route::post('removeGrupoEmpresa', 'API\EmpresaController@removeGrupo');

		Route::post('criaColaborador', 'API\ColaboradorController@store');
		Route::post('atualizaColaborador', 'API\ColaboradorController@update');
		Route::post('deleteColaborador', 'API\ColaboradorController@destroy');

		Route::post('criaSalario', 'API\SalarioController@store');
		Route::post('atualizaSalario', 'API\SalarioController@update');
		Route::post('deleteSalario', 'API\SalarioController@destroy');

//	});
});